//Bullet- object definition for bullets fired from player ship
class Bullet {
  //Bullet position, size and speed variables
  PVector posn;
  PVector size = new PVector(5, 5);
  float speed;

  //Constructor takes in position the bullet will be spawned at. Default speed is set here
  Bullet(PVector position)
  {
    posn = position;
    speed = 10;
  }
 //Move the bullet up;
  //returns true if the bullet is still on screen, false if it's off screen
  //retured boolean will be used to destroy the bullet after it leaves the window
  boolean move() {
    posn.y -= speed;
    return (posn.y > 0);
  }
 //renders the bullet
  void render() {
    fill(196, 255, 218);
    ellipse(posn.x, posn.y, size.x, size.y);
  }
}
