class Enemy {

  //3 corner points for the triangle. will be used starting from the bottom point, going clockwise
  PVector p1, p2, p3;
  color c = color(255, 0, 0); //render emeies in red

  //spped will be random
  float speed;

  //constructor calls the initialize function
  Enemy() {
    initialize();
  }

  //updates enemy position by addding corner points
  void move() {

    {
      p1.y += speed;
      p2.y += speed;
      p3.y += speed;
    }
  }

  //draws the enemy

  void render() {
    strokeWeight(1);
    noStroke();
    fill(c);

    triangle(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
  }

  //this function returns an array of PVectors, to be used in collision checks
  PVector[] getVertices()
  {
    return new PVector[]{p1, p2, p3};
  }

  //initialize sets up the enemy position and speed, and will be called every time the enemy is spawned

  void initialize() {

    //set a random x position  
    float randomX = random(0, 470);

    //assign random speed for variety
    speed = random(4, 7);

    //From the X position, all 3 corner points of the triangle are calculated based on the top of the screen 
    p1 = new PVector(15 + randomX, 0);
    p2 = new PVector(0 + randomX, -30);
    p3 = new PVector(30 + randomX, -30);
  }
}
