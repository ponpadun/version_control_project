//Star- stars used in background

class Star
{
//size and location
PVector size, location;

//speed and brightness- brightness will be used as alpha value in star's colour
float speed, brightness;

  public Star() {
  
    //initialize spawns the star at the top of the screen so we have to change the y position to a random value for when the star is first created. 
    initialize();
    location.y = random(height);
  }
//initialize called every time star is reset
  void initialize() {
       //setting random size. using constrain here to increase chances of getting a small star than a large one.
    //random give me 0 to 6, and any value below 2 will be constrained to 2 and above 5 will be constrained to 5. 
    size =  new PVector(constrain(random(6), 2, 5), constrain(random(6), 2, 5));
    
    //setting random location at the top of the window
    location =  new PVector(random(width), -size.y);
    //calculating speed from size
    speed = (size.x + size.y) /2;

    //calculating random brightness based on speed. Stars with higher speed will have a higher chance of being brigher 
    brightness = constrain(175 + random(speed) * 10, 175, 255);
  }

  //move the star down according to its speed
  void update() {
    location.y += speed;
    if (location.y + size.y/2 >= height) {
      initialize();
    }
  }
  //render the star
  void render() {
    fill(255, 255, 255, brightness);
    ellipse(location.x, location.y, size.x, size.y);
  }
}
